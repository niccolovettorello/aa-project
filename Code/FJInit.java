/*
 * Launcher for the Fisher & Jaikumar algorithm.
 */ 

import util.pp.PP;
import util.common.Result;

public class FJInit {
 
    /*
     * The main function takes the path of the selected data file as argument and passes it to the FJ object constructor.
     * The algorithm is then executed on requested data, and results are collected and pretty-printed.
     */
    public static void main(String[] args) {
        
	String path = "";

	if(args.length == 1) {

		path = args[0];
		System.out.println("You have chosen to run Fisher and Jaikumar algorithm using the data file located at path: " + path);

	}
	else {

		System.out.println("Error in passing arguments");
		return;

	}


	FJ FJExecutor = new FJ(path);

	Result results = FJExecutor.getResults();

	PP prettyPrinter = new PP(results);

	prettyPrinter.print();
    }
}
