/*
 * Launcher for the Savings algorithm.
 */ 

import util.pp.PP;
import util.common.Result;

public class CWInit {
 
    /*
     * The main function takes the path of the selected data file as argument, sanitizes it, and passes it to the CW object constructor.
     * The algorithm is then executed on requested data, and results are collected and pretty-printed.
     */
    public static void main(String[] args) {
        
	String path = "";

	if(args.length == 1) {

		path = args[0];
		System.out.println("You have chosen to run Clarke and Wright algorithm using the data file located at path: " + path);

	}
	else {

		System.out.println("Error in passing arguments");
		return;

	}


	CW CWExecutor = new CW(path);

	Result results = CWExecutor.getResults();

	PP prettyPrinter = new PP(results);

	prettyPrinter.print();
    }
}
