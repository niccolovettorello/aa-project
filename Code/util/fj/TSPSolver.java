/*
 * Solver for the TSP problem. It uses the nearest neighbour euristic.
 */

package util.fj;

import java.util.List;
import java.util.ArrayList;

public class TSPSolver {

	private double[][] edgesCosts;
	private List<List<Integer>> clusters;


	
	private int findNextNode(List<Integer> solution, List<Integer> cluster) {

		int lastNode = solution.get(solution.size() - 1);
		Pair nextNode = new Pair(-1, Double.POSITIVE_INFINITY);
		for(Integer node : cluster) {

			double distance = edgesCosts[lastNode][node];
			if(distance < nextNode.getValue()) {

				nextNode.setKey(node);
				nextNode.setValue(distance);

			}

		}
		return nextNode.getKey();
	}

	private List<Integer> solve(List<Integer> cluster) {

		List<Integer> solution = new ArrayList<>();
		solution.add(0);
		while(!cluster.isEmpty()) {
			
			int nextNode = findNextNode(solution, cluster);
			solution.add(nextNode);
			cluster.remove(cluster.indexOf(nextNode));

		}	
		solution.add(0);
		return solution;

	}


	public TSPSolver(double[][] edgesCosts, List<List<Integer>> clusters) {

		this.edgesCosts = edgesCosts;
		this.clusters = clusters;

	}

	public List<List<Integer>> solveTSP() {

		List<List<Integer>> finalSolution = new ArrayList<>(); 
		clusters.forEach(cluster -> finalSolution.add(solve(cluster)));
		return finalSolution;
	}

}
