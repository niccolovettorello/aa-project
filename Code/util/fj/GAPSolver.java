/*
 * Auxiliary GAP solver for Fisher & Jaikumar algorithm.
 */

package util.fj;

import java.util.List;
import java.util.ArrayList;

public class GAPSolver {

	private int           dimension;
	private int           capacity;
	private List<Integer> demands;
	private int           k;
	private double[][]    clusterCosts;


	/*
	 * Converts the raw GAP solution into an actual list of clusters.
	 */
	private List<List<Integer>> convertToClusters(List<List<Integer>> solution) {

		List<List<Integer>> clusterList = new ArrayList<>();
		for (int x = 0; x < k; x++) {

			List<Integer> cluster = new ArrayList<>();
			for (int y = 0; y < (dimension - 1); y++) {

				int currentInt = solution.get(y).get(x);
				if (currentInt == 1) {

					cluster.add(y + 1);

				}

			}
			clusterList.add(cluster);

		}
		return clusterList;

	}

	/*
	 * Select the next best value and its index.
	 */
	private Pair getNextBestPair(Pair currentPair, int row) {

		double currentClusterCost = currentPair.getValue();
		for (int i = (currentPair.getKey() + 1); i < k; i++) {

			if (clusterCosts[row][i] == currentClusterCost) {

				return new Pair(i, currentClusterCost);

			}

		}
		int bestInd = -1;
		double bestVal = Double.POSITIVE_INFINITY;
		for (int i = 0; i < k; i++) {

			double currentVal = clusterCosts[row][i];
			if ((currentVal > currentClusterCost) && (currentVal < bestVal)) {

				bestInd = i;
				bestVal = currentVal;

			}

		}
		return new Pair(bestInd, bestVal);

	}

	/*
	 * Builds a Row for the solution.
	 */
	private List<Integer> generateRow(int ind) {

		List<Integer> res = new ArrayList<>();
		for (int i = 0; i < k; i++) {

			res.add((i == ind) ? 1 : 0);

		}
		return res;

	}

	/*
	 * True if the current Row doesn't violate constraints, false otherwise.
	 */
	private boolean checkConstraints(List<List<Integer>> solution, int index, int step) {

		int totalDemand = 0;
		for (int i = 0; i < solution.size(); i++) {

			List<Integer> solutionRow = solution.get(i);
			if (solutionRow.get(index) == 1) {

				totalDemand += demands.get(i + 1);

			}

		}
		totalDemand += demands.get(step + 1);
		return totalDemand <= capacity;

	}

	/*
	 * Utility method that actually solves the GAP.
	 */
	private boolean solveGAPRecursively(
		List<List<Integer>> solution,
		int step
	) {

		if (step <= (dimension - 2)) {

			Pair currentPair = new Pair(-1, Double.NEGATIVE_INFINITY);
			for (int i = 0; i < k; i++) {

				currentPair = getNextBestPair(currentPair, step);
				List<Integer> currentRow = generateRow(currentPair.getKey());
				if (checkConstraints(solution, currentPair.getKey(), step)) {

					solution.add(currentRow);
					if (solveGAPRecursively(solution, step + 1)) {

						return true;

					} else {

						solution.remove(solution.size() - 1);

					}

				}

			}
			return false;
		}
		else {

			return true;

		}
	}

	public GAPSolver(
		int dimension, 
		int capacity, 
		List<Integer> demands,
		int k,
		double[][] clusterCosts
	) {

		this.dimension = dimension;
		this.capacity = capacity;
		this.demands = demands;
		this.k = k;
		this.clusterCosts = clusterCosts;

	}

	/*
	 * Solves the GAP and returns a list of clusters.
	 */
	public List<List<Integer>> solveGAP() {

		List<List<Integer>> solution = new ArrayList<>();
		solveGAPRecursively(solution, 0);
		return convertToClusters(solution);

	}

}
