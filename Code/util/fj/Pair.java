/*
 * Represents a pair containing a value of type double and its index within the row.
 */

package util.fj;

public class Pair {

	private int key;
	private double value;

	public Pair(int key, double value) {

		this.key = key;
		this.value = value;

	}

	public int getKey() {

		return key;

	}

	public double getValue() {

		return value;
	
	}

	public void setKey(int key) {

		this.key = key;

	}

	public void setValue(double value) {

		this.value = value;

	}
}
