/*
 * Simple object that represents and stores two coordinates along with the corresponding saving value.
 */
package util.cw;

public class Saving {

	/*
	 * The two nodes.	
	 */
	private int n1;
	private int n2;

	/*
	 * Saving value.
	 */
	private double saving;



	public Saving(int n1, int n2, double saving) {

		this.n1 = n1;
		this.n2 = n2;
		this.saving = saving;

	}

	public void setN1(int n1) {

		this.n1 = n1;

	}

	public void setN2(int n2) {

		this.n2 = n2;

	}

	public void setSaving(double saving) {

		this.saving = saving;

	}

	public int getN1() {

		return n1;

	}

	public int getN2() {

		return n2;

	}

	public double getSaving() {

		return saving;

	}

}
