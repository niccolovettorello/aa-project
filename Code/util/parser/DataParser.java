/*
 * Parser that opens a data file for a CVRP instance and retrieves needed info.
 */

package util.parser;

import java.io.File;
import java.io.FileNotFoundException;

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Queue;
import java.util.LinkedList;

import java.util.stream.Collectors;

import java.lang.Math;

public class DataParser {

	/*
	 * Relative path of the data file.
	 */
	private String filePath;

	/*
	 * The content of the data file.
	 */
	private List<String> fileText;

	/*
	 * Name of the CVRP instance.
	 */
	private String name;

	/*
	 * Trucks' capacity
	 */
	private Integer capacity;

	/*
	 * Dimension of the problem instance, that is the total number of nodes (cities + depots).
	 */
	private Integer dimension;

	/*
	 * Edges'costs matrix.
	 * Note that the matrix dimension is fixed, therefore basic types are used.
	 */
	private double[][] edgesCosts;

	/*
	 * Vector of demands.
	 */
	private List<Integer> demands;

	/*
	 * After initialization, is true if the requested file exists, false otherwise.
	 */
	private boolean fileValidity = false;



	/*
	 * Get the sublist from one value to another.
	 */
	private List<String> getStringList(String from, String to) {

		int startInd = fileText.indexOf(from);
		int endInd = fileText.indexOf(to);

		return fileText.subList(startInd + 1, endInd);

	}

	/*
	 * Private utility method that checks if the path passed as argument leads to an existing entity, and specifically a File (not a Directory).
	 */
	private boolean isFileValid(String path) {

		File requestedFile = new File(path);

		return requestedFile.exists() && !requestedFile.isDirectory();

	}

	/*
	 * Parses problem instance name and instantiates the relative field.
	 */
	private void setName() {

		String[] firstLineSplitted = fileText.get(0).split(":");
		String instanceName = firstLineSplitted[1];
		
		name = instanceName;
	
	}

	
	/*
	 * Parses trucks' capacity and instantiates the relative field.
	 */
	private void setCapacity() {

		List<String> filteredList = fileText
						.stream()
						.filter(line -> line.startsWith("CAPACITY"))
						.collect(Collectors.toList());
		String[] capacityLineSplitted = filteredList.get(0).split(":");
		Integer trucksCapacity = Integer.parseInt(capacityLineSplitted[1].replaceAll("\\s+", ""));

		capacity = trucksCapacity;
	}

	
	/*
	 * Parses problem dimension and instantiates the relative field.
	 */
	private void setDimension() {

		List<String> filteredList = fileText
						.stream()
						.filter(line -> line.startsWith("DIMENSION"))
						.collect(Collectors.toList());
		String[] dimensionLineSplitted = filteredList.get(0).split(":");
		Integer instanceDimension = Integer.parseInt(dimensionLineSplitted[1].replaceAll("\\s+", ""));

		dimension = instanceDimension;
	}

	/*
	 * Parses demands vector and initialized the relative field.
	 */
	private void setDemands() {

		demands = new ArrayList<>();	

		List<String> demandsStringList = getStringList("DEMAND_SECTION", "DEPOT_SECTION");

		demandsStringList.forEach(

			demandString -> {

				String[] demandStringSplitted = demandString.split("\\s+");
				demands.add(Integer.parseInt(demandStringSplitted[(demandStringSplitted.length == 3) ? 2 : 1]));

			}

		);
	
	}

	/*
	 * Parse the array of geographical coordinates.
	 */
	private List<List<Double>> getCoordinates() {

		List<List<Double>> coordinatesList = new ArrayList<>();

		List<String> coordinatesStringList = getStringList("NODE_COORD_SECTION", "DEMAND_SECTION");

		coordinatesStringList.forEach(

			coordinatesString -> {

				List<Double> coordinates = new ArrayList<>();
				String[] coordinatesStringSplitted = coordinatesString.split("\\s+");
				int i = (coordinatesStringSplitted.length == 4) ? 2 : 1;
				coordinates.add(Double.parseDouble(coordinatesStringSplitted[i]));
				coordinates.add(Double.parseDouble(coordinatesStringSplitted[i + 1]));

				coordinatesList.add(coordinates);

			}

		);

		return coordinatesList;

	}

	/*
	 * Converts a coordinate into a degree by using standard formula.
	 */
	private double getRad(double coordinate) {

		return Math.PI * (Math.floor(coordinate) + 5.0 * (coordinate - Math.floor(coordinate)) / 3.0) / 180.0;

	}

	/*
	 * Returns geographical distance.
	 */
	private double getGeoCost(List<Double> p1, List<Double> p2) {

		/*
		 * The first thing to do is to convert coordinates into degrees, and then into radians.
		 */
		List<Double> p1Rad = new ArrayList<>();
		List<Double> p2Rad = new ArrayList<>();
		p1Rad.add(getRad(p1.get(0)));
		p1Rad.add(getRad(p1.get(1)));
		p2Rad.add(getRad(p2.get(0)));
		p2Rad.add(getRad(p2.get(1)));

		/*
		 * Use standard formula to calculate distance.
		 */
		double q1 = Math.cos(p1Rad.get(1) - p2Rad.get(1));
		double q2 = Math.cos(p1Rad.get(0) - p2Rad.get(0));
		double q3 = Math.cos(p1Rad.get(0) + p2Rad.get(0));
		
		return Math.floor(6378.388 * Math.acos(0.5 * ((1.0 + q1) * q2 - (1.0 - q1) * q3)) + 1.0);
	
	}

	/*
	 * Computes the euclidean distances between two points.
	 */
	private double getEuc2DCost(List<Double> p1, List<Double> p2) {

		return Math.floor(
		Math.sqrt(
			Math.pow((p1.get(0) - p2.get(0)), 2)
			+
			Math.pow((p1.get(1) - p2.get(1)), 2)	
		) 
		+ 
		0.5
		);

	}

	/*
	 * Sets the matrix of edges'costs.
	 * NOTE: requires "dimension" field to be set in order to know matrix cardinality.
	 */
	private void setCosts() {

		/*
		 * Extract the edge weight type.
		 */
		List<String> filteredListEWT = fileText
						.stream()
						.filter(line -> line.startsWith("EDGE_WEIGHT_TYPE"))
						.collect(Collectors.toList());
		String[] EWTLineSplitted = filteredListEWT.get(0).split(":");
		String instanceEWT = EWTLineSplitted[1].replaceAll("\\s+", "");

		/*
		 * Instantiate a new empty matrix. This will become a full matrix for costs.
		 */
		edgesCosts = new double[dimension][dimension];

		/*
		 * Fill the main diagonal with zeros.
		 */
		for(int ind = 0; ind < dimension; ind++) {

			edgesCosts[ind][ind] = 0;		

		}

		/*
		 * Handle all various cases.
		 */
		switch(instanceEWT){
			
			case "GEO":

			 	List<List<Double>> geoCoordinatesList = getCoordinates();

				for(int x = 0; x < dimension; x++) {

					for(int y = x + 1; y < dimension; y++){

						double cost = getGeoCost(geoCoordinatesList.get(x), geoCoordinatesList.get(y));
						edgesCosts[x][y] = edgesCosts[y][x] = cost;

					}

				}	
	
				return;

			case "EUC_2D":

				List<List<Double>> euc2DCoordinatesList = getCoordinates();

				for(int x = 0; x < dimension; x++) {

					for(int y = x + 1; y < dimension; y++){

						double cost = getEuc2DCost(euc2DCoordinatesList.get(x), euc2DCoordinatesList.get(y));
						edgesCosts[x][y] = edgesCosts[y][x] = cost;

					}

				}	
	
				return;

			case "EXPLICIT":

				/*
				 * Extract the edge weight format.
				 */
				List<String> filteredListEWF = fileText
								.stream()
								.filter(line -> line.startsWith("EDGE_WEIGHT_FORMAT"))
								.collect(Collectors.toList());
				String[] EWFLineSplitted = filteredListEWF.get(0).split(":");
				String instanceEWF = EWFLineSplitted[1].replaceAll("\\s+", "");
				
				List<String> weightsStringList = (fileText.indexOf("DISPLAY_DATA_SECTION") == -1) ? 
									getStringList("EDGE_WEIGHT_SECTION","DEMAND_SECTION") :
									getStringList("EDGE_WEIGHT_SECTION","DISPLAY_DATA_SECTION") ;
				switch(instanceEWF) {

					case "FULL_MATRIX":

						for(int x = 0; x < dimension; x++) {

							for(int y = 0; y < dimension; y++) {

								String[] weightsSplitted = weightsStringList.get(x).split("\\s+");

								edgesCosts[x][y] = edgesCosts[y][x] = Integer.parseInt(weightsSplitted[y + 1]);

							}
			
						}

						return;

					case "LOWER_DIAG_ROW":

						Queue<Double> costsQueueLDR = new LinkedList<>();

						for(String row : weightsStringList) {
						
							String[] weightsSplitted = row.split("\\s+");

							for(int ind = 0; ind < weightsSplitted.length; ind++) {

								if(!weightsSplitted[ind].isEmpty()){

									costsQueueLDR.add(Double.parseDouble(weightsSplitted[ind]));

								}

							}

						}

						for(int x = 0; x < dimension; x++){

							for(int y = 0; y <= x; y++){

								edgesCosts[x][y] = edgesCosts[y][x] = costsQueueLDR.poll();
							
							}

						}
												
						return;
						
					case "UPPER_ROW":
												
						Queue<Double> costsQueueUR = new LinkedList<>();

						for(String row : weightsStringList) {
						
							String[] weightsSplitted = row.split("\\s+");

							for(int ind = 0; ind < weightsSplitted.length; ind++) {

								if(!weightsSplitted[ind].isEmpty()){

									costsQueueUR.add(Double.parseDouble(weightsSplitted[ind]));

								}

							}

						}

						for(int x = 0; x < dimension; x++) {

							for(int y = x + 1; y < dimension; y++) {

								edgesCosts[x][y] = edgesCosts[y][x] = costsQueueUR.poll();
						
							}

						}	
	
						return;

					default:
						
						System.out.println("Error while parsing edge weight format.");
						
						return;
				
				}

			default:

				System.out.println("Error while parsing edges'costs.");
				return;
		}
	
	}

	/*
	 * Utility method to print all object fields.
	 */
	public void print() {

		System.out.println("NAME: " + name);
		
		System.out.println("DIMENSION: " + dimension);

		System.out.println("CAPACITY: " + capacity);

		System.out.println("DEMANDS:");
		System.out.println(demands);

		System.out.println("COSTS: ");
		for(double[] row : edgesCosts) {

			System.out.println(Arrays.toString(row));

		}

	}

	/*
	 * Constructor for the DataParser: if the file exists changes the appropriate boolean and proceeds with field initialization, 
	 * otherwise does nothing.
	 */
	public DataParser(String path) {
				
		if(isFileValid(path)) {
			
			fileValidity = true;

			File requestedFile = new File(path);

			/*
		 	 * At this point it is assumed that the file is valid.
		 	 */
	
			List<String> textAccumulator = new ArrayList<>();

			try {
			
				/*
				 * Read the file content.
				 */
				Scanner fileScanner = new Scanner(requestedFile);
				
				while(fileScanner.hasNextLine()) {

					String currentLine = fileScanner.nextLine();
					textAccumulator.add(currentLine);
				}

				fileText = textAccumulator;

				/*
				 * Use the file content just read to initialize all fields.
				 */
				setName();
				setCapacity();
				setDimension();
				setDemands();
				setCosts();

				fileScanner.close();
			
			}
			catch(FileNotFoundException e) {

				System.out.println("Something went wrong while trying to scan the file");

			}		
		
		}
		else {

			System.out.println("Requested file is not valid");
		
		}
	}

	/*
	 * Validity status of the parser.
	 */ 
	public boolean isCorrectlyConstructed() {

		return fileValidity;

	}

	/*
	 * Getter for the name.
	 */
 	public String getName() {

		return name;

	}

	/*
	 * Getter for the capacity.
	 */
	public Integer getCapacity() {

		return capacity;

	}

	/*
	 * Getter for the dimension.
	 */
	public Integer getDimension() {

		return dimension;
	
	}

	/*
	 * Getter for the demands.
	 */	
	public List<Integer> getDemands() {

		return demands;

	}

	/*
	 * Getter for the edges' costs.
	 */
	public double[][] getCosts() {

		return edgesCosts;

	}

}
