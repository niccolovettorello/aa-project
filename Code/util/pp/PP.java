/*
 * Pretty printer for algorithm's results.
 */

package util.pp;

import java.util.List;

import util.common.Result;

public class PP {

	/*
	 * The results that must be pretty-printed.
	 */
	private Result results;



	/*
	 * Pretty-printer constructor
	 */
	public PP(Result results) {
	
		this.results = results;

	}

	/*
	 * Pretty-prints the results if they are present. Otherwise it prints an error message.
	 */
	public void print() {

		/*
		 * Extract time and cost data.
		 */

		double minimumTotalCost = results.getCost();
		long totalTime = results.getTime();


		/*
		 * Pretty print the data.
		 */

		System.out.println("The minimum total cost found by the algorithm is: " + minimumTotalCost);

		System.out.println("Total time of execution amounts to: " + totalTime);

		
		/*
		 * Print all found paths
		 */
		List<List<Integer>> routes = results.getRoutes();
		routes.forEach(route -> System.out.println("Route -> " + route));
	}

}
