/*
 * This class contains methods common to both implementations.
 */

package util.common;

import java.util.List;

public class Util {

	/*
	 * Computes total cost of a route.
	 */
	private static double computeCost(List<Integer> route, double[][] edgesCosts) {

		double costAccumulator = 0.0;

		for(int ind = 0; ind < (route.size() - 1); ind++) {

			costAccumulator += edgesCosts[route.get(ind)][route.get(ind + 1)];

		}

		return costAccumulator;

	}

	/*
	 * Computes total cost of a list of routes.
	 */
	public static double computeTotalCost(List<List<Integer>> routes, double[][] edgesCosts) {

		double totalCostAccumulator = 0.0;
		
		for(List<Integer> route : routes) {

			totalCostAccumulator += computeCost(route, edgesCosts);

		}

		return totalCostAccumulator;

	} 	

}
