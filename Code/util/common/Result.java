/*
 * Custom class that represents execution's results.
 */

package util.common;

import java.util.List;
import java.util.ArrayList;

public class Result {

	private double cost;
	private long time;
	private List<List<Integer>> routes;

	

	public Result() {

		cost = -1;
		time = -1L;
		routes = new ArrayList<>();

	}

	public void setCost(double cost) {

		this.cost = cost;

	}

	public void setTime(long time) {

		this.time = time;

	}

	public void setRoutes(List<List<Integer>> routes) {

		this.routes = routes;

	}

	public double getCost() {

		return cost;

	}

	public long getTime() {

		return time;

	}
	
	public List<List<Integer>> getRoutes() {

		return routes;

	}
}
