/*
 * Java implementation of the Savings algorithm by Clarke and Wright (1964), designed to solve CVRP.
 */

import util.parser.DataParser;
import util.cw.Saving; 
import util.common.Result;
import util.common.Util;

import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

import java.lang.Math;

public class CW {

	/*
	 * Object's fields consists in all the useful info that can be retrieved, directly or indirectly, from the data file.
	 * There is also a field which stores the results obtained by running the algorithm.
	 */


	/*
         * Name of the problem instance.
	 */
	private String name;

	/*
	 * Dimension of the problem instance, that is the total number of nodes (cities + depots).
	 */
	private Integer dimension;

	/*
	 * Trucks' capacity.
	 */
	private Integer capacity;	

	/*
	 * Edges'costs matrix.
	 * Note that the matrix dimension is fixed, therefore basic types are used.
	 */
	private double[][] edgesCosts;

	/*
	 * Vector of demands.
	 */
	private List<Integer> demands;

	private Result results;

	

	/*
	 * Check if the total demand of the route is invalid.
	 */
	private boolean isRouteDemandInvalid(List<Integer> route) {

		int demandsAccumulator = 0;

		for(int ind = 0; ind < route.size(); ind++) {

			demandsAccumulator += demands.get(route.get(ind));
	
		}

		return demandsAccumulator > capacity;

	}

	/*
	 * Merge paths if possible.
	 */
	private void mergePaths(Saving saving, List<List<Integer>> routesList) {

		/*
		 * 1: the two points are not on the same route
		 */
		for(List<Integer> route : routesList) {

			if(route.contains(saving.getN1()) && route.contains(saving.getN2())) {

				return;

			}	

		}

		/*
		 * 2: two routes, one starting with {0, n2, ...} and the other ending with {..., n1, 0}, exist effectively
		 */
		int n2RouteInd = -1;
		int n1RouteInd = -1;
		for(int ind = 0; ind < routesList.size(); ind++) {

			List<Integer> route = routesList.get(ind);

			if((route.get(0) == 0) && (route.get(1) == saving.getN2())) {

				n2RouteInd = ind;	

			}

		}
		for(int ind = 0; ind < routesList.size(); ind++) {

			List<Integer> route = routesList.get(ind);

			if((route.get(route.size() - 1) == 0) && (route.get(route.size() - 2) == saving.getN1())) {

				n1RouteInd = ind;	

			}

		}
		if((n2RouteInd == -1) || (n1RouteInd == -1)) {

			return;

		}

		/*
		 * Build the resulting merged list
		 */
		List<Integer> mergedRoute = new ArrayList<>();
		for(int ind = 0; ind < (routesList.get(n1RouteInd).size() - 1); ind++) {

			mergedRoute.add(routesList.get(n1RouteInd).get(ind));
		
		}
		for(int ind = 1; ind < (routesList.get(n2RouteInd).size()); ind++) {

			mergedRoute.add(routesList.get(n2RouteInd).get(ind));

		}

		/*
		 * 3: Checking that capacity constraint is not violated by the list just built
		 */
		if(isRouteDemandInvalid(mergedRoute)) {

			return;

		}

		/*
		 * All checks passed, the two lists can effectively be merged
		 */
		routesList.remove(Math.min(n1RouteInd, n2RouteInd));
		routesList.remove(Math.max(n1RouteInd, n2RouteInd) - 1);
		routesList.add(mergedRoute);

	}

	/*
	 * Executes CW algorithm.
	 */
	private void execute() {

		/*
		 * STEP 1: create a list containing (dimension - 1) different routes: this is the worst case scenario to be optimized
		 * NOTE: a route is a list of integers in the form {0, ..., 0}, meaning that it's a cycle that starts and ends at the depot
		 */
		List<List<Integer>> routesList = new ArrayList<>();
		for(int ind = 1; ind < dimension; ind++) {

			List<Integer> route = new ArrayList<>();
			route.add(0);
			route.add(ind);
			route.add(0);

			routesList.add(route);

		}

		/*
		 * STEP 2: compute the savings list
		 * For every i, j >= 1 such that i != j compute the following value:
		 * Sij = (Ci0 + C0j) - Cij
		 */
		List<Saving> savingsList = new ArrayList<>();
		for(int n1 = 1; n1 < dimension; n1++) {

			for(int n2 = 1; n2 < dimension; n2++) {

				if(n1 != n2) {
				
					double saving = (edgesCosts[n1][0] + edgesCosts[0][n2]) - edgesCosts[n1][n2];
					savingsList.add(new Saving(n1, n2, saving));		
		
				}

			}

		}

		/*
		 * STEP 3: sort the list just created by the value of the saving, in descending order
		 */
		savingsList.sort(Comparator.comparing(Saving::getSaving));
		Collections.reverse(savingsList);

		/*
		 * STEP 4: visit the list and merge paths if possible
		 */
		savingsList.forEach(

			saving -> mergePaths(saving, routesList)

		);

		/*
		 * STEP 5: compute total cost
		 */
		results.setCost(Util.computeTotalCost(routesList, edgesCosts));

		/*
		 * STEP 6: add the found routes to the results
		 */
		results.setRoutes(routesList);

	}

	/*
	 * The constructor takes the relative path for the selected data file, instantiates the parser object and initializes private fields.
	 */
	public CW(String path) {
		
		DataParser dataParser = new DataParser(path);

		if(dataParser.isCorrectlyConstructed()){
			
			name = dataParser.getName();
			dimension = dataParser.getDimension();
			capacity = dataParser.getCapacity();
			edgesCosts = dataParser.getCosts();
			demands = dataParser.getDemands();

			results = new Result();
		}
		else
			System.out.println("Error reading data");

	}

	/*
	 * Executes the algorithm and returns computed results.
	 */
	public Result getResults() {

		long startTimeMillis = System.currentTimeMillis();

		execute();

		long endTimeMillis = System.currentTimeMillis();

		results.setTime(endTimeMillis - startTimeMillis);

		return results;
	}

}

