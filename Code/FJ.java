/*
 * Java implementation of the algorithm by Fisher and Jaikumar (1981), designed to solve CVRP.
 */

import util.parser.DataParser;

import util.common.Result;
import util.common.Util;

import util.fj.GAPSolver;
import util.fj.TSPSolver;

import java.util.List;
import java.util.ArrayList;
import java.util.Random;

import java.lang.Math;

public class FJ {

	/*
	 * Object's fields consists in all the useful info that can be retrieved, directly or indirectly, from the data file.
	 * There is also a field which stores the results obtained by running the algorithm.
	 */


	/*
         * Name of the problem instance.
	 */
	private String name;

	/*
	 * Dimension of the problem instance, that is the total number of nodes (cities + depots).
	 */
	private Integer dimension;

	/*
	 * Trucks' capacity.
	 */
	private Integer capacity;	

	/*
	 * Edges'costs matrix.
	 * Note that the matrix dimension is fixed, therefore basic types are used.
	 */
	private double[][] edgesCosts;

	/*
	 * Vector of demands.
	 */
	private List<Integer> demands;

	private Result results;

	

	/*
	 * Computes the matrix of cluster costs.
	 */
	private double[][] computeClusterCosts(int k, List<Integer> seeds) {

		double[][] clusterCosts = new double[dimension - 1][k];
		for(int x = 0; x < (dimension - 1); x++) {

			for(int y = 0; y < k; y++) {

				int currentSeed = seeds.get(y);
				clusterCosts[x][y] = 
					(edgesCosts[0][x + 1] + edgesCosts[x + 1][currentSeed] + edgesCosts[currentSeed][0]) - 
					(edgesCosts[0][currentSeed] * 2);

			}

		}
		return clusterCosts;

	}
 
	/*
	 * Executes CW algorithm.
	 */
	private List<List<Integer>> execute(int k) {

		/*
		 * STEP 1: select randomly k nodes between 1 and n to initialize clusters
		 */
		List<Integer> seeds = new ArrayList<>();
		Random generator = new Random();
		while(seeds.size() < k) {

			int n = generator.nextInt(dimension - 1) + 1;
			if(!seeds.contains(n)) {

				seeds.add(n);
	
			}		
			
		}

		//System.out.println(seeds);

		/*
		 * STEP 2: build up the cluster costs matrix
		 */
		double[][] clusterCosts = computeClusterCosts(k, seeds);
		/*
		for(int x = 0; x < (dimension - 1); x++) {

			for(int y = 0; y < k; y++){

				System.out.println(clusterCosts[x][y]);

			}

		}
		*/

		/*
		 * STEP 3: solve the GAP using the matrix just generated
		 */
		GAPSolver GAPsolver = new GAPSolver(dimension, capacity, demands, k, clusterCosts);
		List<List<Integer>> clusters = GAPsolver.solveGAP();

		/*
		 * STEP 4: solve TSP for every list of Nodes
		 */
		TSPSolver TSPsolver = new TSPSolver(edgesCosts, clusters);
		return TSPsolver.solveTSP();
		
	}

	/*
	 * The constructor takes the relative path for the selected data file, instantiates the parser object and initializes private fields.
	 */
	public FJ(String path) {
		
		DataParser dataParser = new DataParser(path);

		if(dataParser.isCorrectlyConstructed()){
			
			name = dataParser.getName();
			capacity = dataParser.getCapacity();
			edgesCosts = dataParser.getCosts();
			demands = dataParser.getDemands();
			dimension = dataParser.getDimension();

			results = new Result();

		}
		else
			System.out.println("Error reading data");

	}

	/*
	 * Executes the algorithm and returns computed results.
	 */
	public Result getResults() {

		long startTimeMillis = System.currentTimeMillis();
		
		/*
		 * We compute the number of clusters from the minimum value to the maximum one (i.e. dimension - 1), and execute the algorithm with each value, keeping
		 * the best result
		 */
		int totalDemand = 0;
		for(Integer demand : demands) {

			totalDemand += demand;
		
		}
		int k = totalDemand / capacity;
		k = k + ((totalDemand % capacity == 0) ? 0 : 1);

		double smallestTotalCost = Double.POSITIVE_INFINITY;
		List<List<Integer>> bestRoutesList = new ArrayList<>();
		for(int i = k; i < dimension; i++) {

			List<List<Integer>> routes = execute(i);
			double currentTotalCost = Util.computeTotalCost(routes, edgesCosts);
			if(currentTotalCost < smallestTotalCost) {

				smallestTotalCost = currentTotalCost;
				bestRoutesList = routes;

			}

		}
		results.setCost(smallestTotalCost);
		results.setRoutes(bestRoutesList);		

		long endTimeMillis = System.currentTimeMillis();

		results.setTime(endTimeMillis - startTimeMillis);

		return results;
	}

}

