# Capacitated Vehicle Routing Problem

Move into _Code_ directory using the command

`cd Code`


To build and execute the _Clarke and Wright_ algorithm (for example on the _ulysses-n16-k3.vrp_ instance) use the commands

`javac CWInit.java`

`java CWInit Data/ulysses-n16-k3.vrp`


To build and execute the _Fisher and Jaikumar_ algorithm (for example on the _ulysses-n16-k3.vrp_ instance) use the commands

`javac FJInit.java`

`java FJInit Data/ulysses-n16-k3.vrp`


To clean all _.class_ files after compilation use the commands

`sudo chmod +x clean.sh`

`./clean.sh`
